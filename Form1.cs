﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ResultProcessingSystem
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void saveButton_Click(object sender, EventArgs e)
        {

            string studentRollNo,
                studentName,
                softwareEngineeringMarks,
                compilerDesignMarks,
                numericalAnalysisMarks,
                softwareEngineeringGrade,
                compilerDesignGrade,
                numericalAnalysisGrade;
            double sumofPoint;
            studentRollNo = studentRollNoTextBox.Text;
            studentName = StudentNameTextBox.Text;
            softwareEngineeringMarks = softwareEngineeringMarksTextBox.Text;
            compilerDesignMarks = compilerDesignMarksTextBox.Text;
            numericalAnalysisMarks = numericalAnalysisMarksTextBox.Text;
            softwareEngineeringGrade = GradeResult(GradePoint(Convert.ToDouble(softwareEngineeringMarks)));
            compilerDesignGrade = GradeResult(GradePoint(Convert.ToDouble(compilerDesignMarks)));
            numericalAnalysisGrade = GradeResult(GradePoint(Convert.ToDouble(numericalAnalysisMarks)));
            sumofPoint = GradePoint(Convert.ToDouble(softwareEngineeringMarks)) +
                         GradePoint(Convert.ToDouble(compilerDesignMarks)) +
                         GradePoint(Convert.ToDouble(numericalAnalysisMarks));
            sumofPoint /= 3;
            if (sumofPoint < 2)
                sumofPoint = 0;
            string connectionString =
                @"SERVER=.\SQLEXPRESS;DATABASE=ResultProcessingSystem;INTEGRATED SECURITY = SSPI";
            SqlConnection connection = new SqlConnection(connectionString);
            string query = "INSERT INTO ResultInformationTable VALUES('" + studentRollNo + "','" + studentName +
                           "','" + softwareEngineeringMarks + "','" + compilerDesignMarks + "','" +
                           numericalAnalysisMarks + "','" + softwareEngineeringGrade + "','" + compilerDesignGrade +
                           "','" + numericalAnalysisGrade + "','" + GradeResult(sumofPoint) + "','" +
                           Convert.ToString(sumofPoint) + "')";
            SqlCommand insertQuery = new SqlCommand(query, connection);
            connection.Open();
            insertQuery.ExecuteNonQuery();
            connection.Close();
            MessageBox.Show("Saved Successfully! ");
            studentRollNoTextBox.Text =
                StudentNameTextBox.Text =
                    softwareEngineeringMarksTextBox.Text =
                        compilerDesignMarksTextBox.Text = numericalAnalysisMarksTextBox.Text = null;


        }

        public double GradePoint(double marks)
        {
            if (marks >= 80)
                return 4.0;
            else if (marks >= 75) return 3.75;
            else if (marks >= 70) return 3.50;
            else if (marks >= 65) return 3.25;
            else if (marks >= 60) return 3.0;
            else if (marks >= 55) return 2.75;
            else if (marks >= 50) return 2.50;
            else if (marks >= 45) return 2.25;
            else if (marks >= 40) return 2.0;
            else return 0.0;
        }

        public string GradeResult(double point)
        {
            if (point == 4.0) return "A+";
            else if (point == 3.75) return "A";
            else if (point == 3.50) return "A-";
            else if (point == 3.25) return "B+";
            else if (point == 3.00) return "B";
            else if (point == 2.75) return "B-";
            else if (point == 2.50) return "C+";
            else if (point == 2.25) return "C";
            else if (point == 2.00) return "D";
            else return "F";
        }

        private void showButton_Click(object sender, EventArgs e)
        {
            Display newFun=new Display();
            this.Hide();
            newFun.Closed += (s, args) => this.Close();
            newFun.Show();

        }



    }
}

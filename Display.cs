﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ResultProcessingSystem
{
    public partial class Display : Form
    {
        public Display()
        {
            InitializeComponent();
        }

        private void Exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void ResultPublish(List<ResultStore> newResultProcess)
        {
            studentResultInformationListView.Items.Clear();
            foreach (var resultStore in newResultProcess)
            {
                ListViewItem listViewI1=new ListViewItem(resultStore.studentRollNo.ToString());
                
                listViewI1.SubItems.Add(resultStore.studentName);
                listViewI1.SubItems.Add(resultStore.softwareEngineeringMarks);
                listViewI1.SubItems.Add(resultStore.compilerDesignMarks);
                listViewI1.SubItems.Add(resultStore.numericalAnalysisMarks);
                listViewI1.SubItems.Add(resultStore.softwareEngineeringGrade);
                listViewI1.SubItems.Add(resultStore.compilerDesignGrade);
                listViewI1.SubItems.Add(resultStore.numericalAnalysisGrade);
                listViewI1.SubItems.Add(resultStore.finalGradeAchieve);
                listViewI1.SubItems.Add(resultStore.GradePoint);
                studentResultInformationListView.Items.Add(listViewI1);

            }
        }

        private void Display_Load(object sender, EventArgs e)
        {
            string connectionString = @"SERVER=.\SQLEXPRESS;DATABASE=ResultProcessingSystem;INTEGRATED SECURITY=TRUE";
            SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();
            string query = "SELECT * FROM ResultInformationTable";
            SqlCommand cmd= new SqlCommand(query,connection);
            SqlDataReader reader = cmd.ExecuteReader();
            List<ResultStore> listResult = new List<ResultStore>();
            while (reader.Read())
            {
                ResultStore newResult = new ResultStore();
                newResult.studentRollNo = reader[0].ToString();
                newResult.studentName = reader[1].ToString();
                newResult.softwareEngineeringMarks = reader[2].ToString();
                newResult.compilerDesignMarks = reader[3].ToString();
                newResult.numericalAnalysisMarks = reader[4].ToString();
                newResult.softwareEngineeringGrade = reader[5].ToString();
                newResult.compilerDesignGrade = reader[6].ToString();
                newResult.numericalAnalysisGrade = reader[7].ToString();
                newResult.finalGradeAchieve = reader[8].ToString();
                newResult.GradePoint = reader[9].ToString();
                listResult.Add(newResult);

            }
            reader.Close();
            connection.Close();
            ResultPublish(listResult);
           
        }
    }
}
